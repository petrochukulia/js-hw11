// 1. Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища

class NewUser {
	constructor(name, sname) {
		this.name = name,
			this.sname = sname,
			this.show = function () {
				console.log(`${this.name} ${this.sname}`);
				return (`${this.name} ${this.sname}`);
			};
	}
}
const user1 = new NewUser("Ivan", "Petrov");
document.getElementById("task1").append(user1.show());

// 2. Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 жовтий

let firstEl = document.querySelector("li");
firstEl.style.backgroundColor = "blue";
let thirdEl = document.querySelector("ul").childNodes[5];
thirdEl.style.backgroundColor = "yellow";

// 3. Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

window.addEventListener("DOMContentLoaded", ()=>{
	const blueDiv = document.getElementById("task3");

blueDiv.onmousemove = function (e){
	this.innerHTML = `Подія onmouse Х: ${e.offsetX}, Y: ${e.offsetY}`
}
})

// 4. Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

let main = document.getElementById("task4");
main.addEventListener("click", (e) =>{
	let message = document.createElement("p");
	document.getElementById("task4").append(message);
switch(e.target.value){
	case"1": 
	message.textContent = "Натиснута кнопка 1";
	break;
	case"2": 
	message.textContent = "Натиснута кнопка 2";
	break;
	case"3": 
	message.textContent = "Натиснута кнопка 3";
	break;
	case"4": 
	message.textContent = "Натиснута кнопка 4";
	break;
}

});
// 5. Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
window.addEventListener("DOMContentLoaded", ()=>{
let runSquare = document.getElementById("task5");
runSquare.onmousemove = function (e){
	let positionY = Math.floor(Math.random()*100);
	let positionX = Math.floor(Math.random()*100)
	runSquare.style.top = positionY + "vh";
	runSquare.style.left = positionX + "vw";
}
})

// 6. Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
task6color.addEventListener("input", updateFirst, false);
function updateFirst(event) {
  let b = document.getElementById("b1");
	b.style.backgroundColor = event.target.value;
}

//7. Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

const loginField = document.createElement("input");
let task7 = document.getElementById("task7");
task7.append(loginField);

loginField.addEventListener("keypress", function(e){
	console.log(loginField.value)
});

//8. Створіть поле для введення даних у полі введення даних виведіть текст під полем

const textField = document.createElement("textarea");
const showText = document.createElement("p");
document.body.append(textField);
document.body.append(showText);

textField.addEventListener("keypress", function(e){
	showText.innerText = textField.value;
})






